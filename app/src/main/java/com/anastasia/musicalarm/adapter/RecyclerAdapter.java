package com.anastasia.musicalarm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anastasia.musicalarm.AlarmDetailed;
import com.anastasia.musicalarm.R;
import com.anastasia.musicalarm.alarm.Alarm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anastasia_Paramonova on 16.04.2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecycleViewHolder>{
    public static final String SONG_TITLE_EXTRA = "com.anastasia.musicalarm.adapter._SONGTITLE";
    public static final String WAKE_TIME_EXTRA = "com.anastasia.musicalarm.adapter._WAKETIME";
    public static final String SONG_LIST_EXTRA = "com.anastasia.musicalarm.adapter._SONGLIST";

    private ArrayList<Alarm> alarms;
    private Context context;

    public RecyclerAdapter(Context context){
        alarms = new ArrayList<>();
        this.context = context;
    }

    @Override
    public RecyclerAdapter.RecycleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new RecyclerAdapter.RecycleViewHolder(view, context, alarms);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.RecycleViewHolder holder, int position) {
        holder.bind(alarms.get(position));
    }

    @Override
    public int getItemCount() {
        return this.alarms.size();
    }

    public void addAll(List<Alarm> alarms) {

        int count = getItemCount();
        this.alarms.addAll(alarms);
        this.alarms.get(13).activate(context);
        this.alarms.get(14).activate(context);

        notifyItemChanged(count, this.alarms.size());
    }

    public class RecycleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView songTitle;
        private TextView wakeTime;
        private Context context;
        private List<Alarm> alarms;
        private ArrayList<String> songTitles;

        public RecycleViewHolder(View itemView, Context context, List<Alarm> alarms) {
            super(itemView);
            this.context = context;
            this.alarms = alarms;
            songTitles = getSongTitles(alarms);

            itemView.setOnClickListener(this);
            songTitle = (TextView) itemView.findViewById(R.id.song_title);
            wakeTime = (TextView) itemView.findViewById(R.id.wake_time);
        }

        public void bind(Alarm alarm) {
            songTitle.setText(alarm.getSongTitle());
            wakeTime.setText(alarm.getWakeTime());
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            Alarm alarm = this.alarms.get(position);
            Intent intent = new Intent(this.context, AlarmDetailed.class);

            intent.putExtra(SONG_TITLE_EXTRA, alarm.getSongTitle());
            intent.putExtra(WAKE_TIME_EXTRA, alarm.getWakeTime());
            intent.putExtra(SONG_LIST_EXTRA, songTitles);

            this.context.startActivity(intent);
        }

        private ArrayList<String> getSongTitles(List<Alarm> alarms){
            ArrayList<String> titles = new ArrayList<>(alarms.size());
            for(Alarm alarm : alarms){
                titles.add(alarm.getSongTitle());
            }
            return titles;
        }
    }
}

