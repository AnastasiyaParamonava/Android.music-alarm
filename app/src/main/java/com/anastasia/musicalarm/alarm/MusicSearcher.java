package com.anastasia.musicalarm.alarm;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.anastasia.musicalarm.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anastasia_Paramonova on 16.04.2017.
 */

public class MusicSearcher {

    private ContentResolver contentResolver;

    public MusicSearcher(ContentResolver contentResolver){
        this.contentResolver = contentResolver;
    }

    public List<Song> getMusic(){
        ArrayList<Song> songs = new ArrayList<>();

        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor songCursor = contentResolver.query(songUri, null, null, null, null);

        if(songCursor != null && songCursor.moveToFirst()){
            int songTitleColumn = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songIdColumn = songCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            do{
                String songTitle = songCursor.getString(songTitleColumn);
                long songId = songCursor.getLong(songIdColumn);
                songs.add(new Song(songTitle, songId));
            } while (songCursor.moveToNext());
        }

        songCursor.close();
        return songs;
    }
}
