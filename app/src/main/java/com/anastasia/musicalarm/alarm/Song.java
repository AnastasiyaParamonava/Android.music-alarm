package com.anastasia.musicalarm.alarm;

/**
 * Created by Anastasia_Paramonova on 16.04.2017.
 */

public class Song {
    private String title;
    private long id;

    public Song(String title, long id){
        this.title = title;
        this.id = id;
    }

    public String getTitle(){
        return title;
    }

    public long getId(){
        return id;
    }
}
